package com.atlassian.pom;

import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageRestrictions 
{

	WebDriver driver;


	public PageRestrictions(WebDriver driver)
	{
		this.driver=driver;
	}


	@FindBy(how=How.XPATH,using="//*[@id=\"system-content-items\"]/div/span/div/button")
	@CacheLookup
	WebElement restrictionLock;
	
	
	@FindBy(how=How.XPATH,using="//*[@id=\"com-atlassian-confluence\"]/div[2]/div[7]/div/div[3]/div[2]/div/div/div/div/div/div[1]/div[1]/div/div")
	@CacheLookup
	WebElement listBox;

	public void clickRestrictions() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(restrictionLock));
		restrictionLock.click();
	}
	
	public boolean isRestrictionListBoxPresent() throws InterruptedException
	{
		try
		{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(listBox));
			listBox.click();
			return true;
		}
		catch(ElementClickInterceptedException e)
		{
			System.out.println("ElementClickInterceptedException in isRestrictionListBoxPresent  returning false");
			return false;
		}
		
	}

}