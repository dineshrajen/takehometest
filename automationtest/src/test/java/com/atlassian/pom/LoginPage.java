package com.atlassian.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage 
{

	WebDriver driver;

	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
	}


	@FindBy(id="username") 
	@CacheLookup
	WebElement username; 

	@FindBy(how=How.ID,using="password")
	@CacheLookup
	WebElement password;

	@FindBy(how=How.ID,using="login-submit")
	@CacheLookup
	WebElement submit_button;
	
	


	public void login(String uid,String pass) throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);
		username.sendKeys(uid);
		submit_button.click();
		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys(pass);
		submit_button.click();
		
		wait.until(ExpectedConditions.titleContains("Atlassian | Start page"));
	}


}