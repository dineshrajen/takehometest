package com.atlassian.test;

import junit.framework.Assert;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.atlassian.pom.LoginPage;
import com.atlassian.pom.PageRestrictions;


// set the chromedriver path in "webdriver.chrome.driver" system parameter
public class TestRestrictionsForuser 
{

	@DataProvider(name = "Restrictions")
	 
	  public static Object[][] credentials() {
	        return new Object[][] { { "dinesh240681@gmail.com", "PpFJkxK4WGi3","https://dindina.atlassian.net/wiki/spaces/MYOWN/pages/262145/New+page" },
	         { "ytfordinesh@gmail.com", "password" ,"https://dindina.atlassian.net/wiki/spaces/MYOWN/pages/262145/New+page"}
	        };
	 
	 }
	@Test(dataProvider = "Restrictions")
	public void checkRestrictions(String userid, String password, String pageurl)
	{
		if(System.getProperty("webdriver.chrome.driver") == null)
		{
			System.setProperty("webdriver.chrome.driver", "/Users/dirajendran/diNa/atlassian takehome exercise/chromedriver");
		}
		WebDriver driver=new ChromeDriver();
		try
		{
			driver.manage().window().maximize();
			driver.get("https://id.atlassian.com/login");
			LoginPage login_page=PageFactory.initElements(driver, LoginPage.class);
			login_page.login(userid,password);
			driver.get(pageurl);
			PageRestrictions PageRestrictions_page = PageFactory.initElements(driver, PageRestrictions.class);
			PageRestrictions_page.clickRestrictions();
			Assert.assertTrue(PageRestrictions_page.isRestrictionListBoxPresent());

		}
		catch(Exception e)
		{
			System.out.println("Exception in test " + e);
			e.printStackTrace();
			Assert.fail();
		}
		finally
		{
			driver.quit();
		}

	}



}